---
title: "Introduction to R and RStudio"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

This material is taken and adapted from http://swcarpentry.github.io/r-novice-gapminder/01-rstudio-intro/
and licensed under CC BY 4.0.

teaching: 28

exercises: 20

total: 55 (at 10:25)

questions:

- "How to find your way around RStudio?"
- "How to interact with R?"
- "How to install packages?"

objectives:

- "Describe the purpose and use of each pane in the RStudio IDE"
- "Locate buttons and options in the RStudio IDE"
- "Define a variable"
- "Assign data to a variable"
- "Use mathematical and comparison operators"
- "Call functions"
- "Manage packages"

keypoints:

- "Use RStudio to write and run R programs."
- "R has the usual arithmetic operators and mathematical functions."
- "Use `<-` to assign values to variables."
- "Use `install.packages()` to install packages."
- "Use `library()` to load packages."



## Motivation

Science is a multi-step process: once you've designed an experiment and collected
data, the real fun begins! This lesson will teach you how to start this process using
R and RStudio. We will 

- use R as a calculator
- read data
- manipulate data and
- plot results graphically. 

We will be mostly working with a data set from
[gapminder.org](https://www.gapminder.org) containing population information for many
countries through time. 


## Introduction to RStudio


We'll be using RStudio: a free, open source R integrated development
environment. RStudio is an editor for R. It is where you can write R 
code and run it.



**Basic layout**

When you first open RStudio, you will be greeted by three panels:

  * The interactive R console (entire left)
  * Environment/History (tabbed in upper right)
  * Files/Plots/Packages/Help/Viewer (tabbed in lower right)

![RStudio layout](../fig/01-rstudio.png)

Once you open files, such as R scripts, an editor panel will also open
in the top left.

![RStudio layout with .R file open](../fig/01-rstudio-script.png)


# Challenge 1


## Work flow within RStudio

- Write code in R Script.
- Run it using `Run` button or `Ctrl + Enter`

Or 

- Try things in the console until it works
- Copy-paste the code to R Script.



## Using R as a calculator

The simplest thing you could do with R is do arithmetic:

```{r}
1 + 100
```

And R will print out the answer.

If you type in an incomplete command, R will wait for you to
complete it:

~~~
> 1 +
+
~~~


If the R session shows a "+" instead of a ">", it
means it's waiting for you to complete the command. If you want to cancel
a command you can simply hit "Esc" and RStudio will give you back the ">"
prompt.



When using R as a calculator, the order of operations is the same as you
would have learned back in school.


```{r}
3 + 5 * 2  # same as 3 + (5 * 2)
```

versus 

```{r}
(3 + 5) * 2
```

The text after each line of code is called a
"comment". Anything that follows after the hash (or octothorpe) symbol
`#` is ignored by R when it executes code.

```{r}
# just typing a comment
```


Really small or large numbers get a scientific notation:

```{r}
2/10000
```

Which is shorthand for "multiplied by `10^XX`". So `2e-4`
is shorthand for `2 * 10^(-4)`.



## Mathematical functions

R has many built in mathematical functions. To call a function,
we simply type its name, followed by  open and closing parentheses.
Anything we type inside the parentheses is called the function's
arguments:

```{r}
sin(1)  # trigonometry functions
```

```{r}
log(1)  # natural logarithm
```

```{r}
exp(0.5) # e^(1/2)
```

Don't worry about trying to remember every function in R. 

Pro tips: 

- Use **tab completion in RStudio**.
- Use `?` before the name of a command will open the help page
for that command.

```{r, eval=FALSE}
?log
```



## Comparing things

We can also do comparison in R:

```{r}
1 == 1  # equality (note two equals signs, read as "is equal to")
```

```{r}
1 != 2  # inequality (read as "is not equal to")
```

```{r}
1 < 2  # less than
```

```{r}
1 <= 1  # less than or equal to
```

```{r}
1 > 0  # greater than
```

```{r}
1 >= -9 # greater than or equal to
```

Pro Tip: Alway compare numbers with all.equal!
```{r}
1/3
0.1/0.3
1/3 == 0.1/0.3
all.equal(1/3, 0.1/0.3)
```

## Variables and assignment

We can store values in variables using the assignment operator `<-`, like this:

```{r}
x <- 1/40
```

Notice that assignment does not print a value. Instead, we stored it for later
in something called a **variable**. `x` now contains the **value** `0.025`:

```{r}
x
```

Look for the `Environment` tab in one of the panes of RStudio, and you will see that `x` and its value
have appeared. Our variable `x` can be used in place of a number in any calculation that expects a number:

```{r}
x + 1
```

Notice also that variables can be reassigned:

```{r}
x <- 100
```

`x` used to contain the value 0.025 and and now it has the value 100.

Assignment values can contain the variable being assigned to:

```{r}
x <- x + 1 #notice how RStudio updates its description of x on the top right tab
y <- x * 2
```

Variable names can contain letters, numbers, underscores and periods. They
cannot start with a number nor contain spaces at all. 



## Challenge 2

## Vectors

R variables and functions can have vectors as values. 
A vector in R describes a set of values in a certain order of the 
same data type. For example

```{r}
1:5
2^(1:5)
x <- 1:5
2^x

y <- c(1, 1, 2, 2, 2)
y + 1
```



## R Packages

It is possible to add functions to R by writing a package, or by
obtaining a package written by someone else. There
are over 10,000 packages available on CRAN (the comprehensive R archive
network). 

Important functions:

* You can install packages by typing `install.packages("packagename")`,
  where `packagename` is the package name, in quotes.
* You can make a package available for use with `library(packagename)`

## Challenge 3 - 5


## Project organisation

I usually organize my projects by having different folders for different things.

The main project folder contains:

- **Files** for writing the manuscript
- Folder `data_raw`
- Folder `data_clean`
- Folder `analysis`


## Challenge 6

